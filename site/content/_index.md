+++

[[student]]
name = "Ariana Marta"
link = "https://arianamarta.gitlab.io/digital-fabrication/"

[[student]]
name = "Burak Türköz"
link = "https://burakturkoz.gitlab.io/digital-fabrication-by-burak/"

[[student]]
name = "Damla Serper"
link = "https://damlaserper.gitlab.io/digital-fabrication/"

[[student]]
name = "Dikai Yu"
link = "https://yudikai233.gitlab.io/test001/"

[[student]]
name = "Hiski Huovila"
link = "https://hiskihuovila.gitlab.io/digital-fabrication/"

[[student]]
name = "Richard Van der Walt"
link = "https://rwaltaalto.gitlab.io/DigitalFabrication/"

[[student]]
name = "Sara Kutkova"
link = "https://sarisko.gitlab.io/digital-fabrication/"

[[student]]
name = "Saskia Helinska"
link = "https://fabacademy.org/2023/labs/aalto/students/saskia-helinska/"

[[student]]
name = "Zhicheng Wang"
link = "https://zhicheng_wang.gitlab.io/"

[[student]]
name = "Karl Mihhels"
link = "https://karlpalo.gitlab.io/fablab2022/"

+++

Welcome to Digital Fabrication 2023 final projects page! Click on the image thumbnails to access videos.
